import View from './View.js';

export default class WelcomeView extends View {
	socket;
	loginForm;
	imagePreview;
	fileImagePicker;

	constructor(element, loginForm, imagePreview, fileImagePicker, socket) {
		super(element);
		this.socket = socket;
		this.loginForm = loginForm;
		this.fileImagePicker = fileImagePicker;
		this.imagePreview = imagePreview;

		this.fileImagePicker.on('change', () => {
			const files = this.fileImagePicker.get(0).files[0];
			if (files) {
				const fileReader = new FileReader();
				fileReader.readAsDataURL(files);
				fileReader.addEventListener('load', function () {
					imagePreview.show();
					imagePreview.html('<img src="' + this.result + '" />');
				});
			}
		});

		loginForm.on('submit', event => {
			event.preventDefault();

			const username = loginForm.find('.username').val();
			const color = loginForm.find('.player-color').val();
			const image = fileImagePicker.get(0).files[0];

			new Response(image).arrayBuffer().then(arrayImage => {
				const base64Image = arrayBufferToBase64(arrayImage);
				this.socket.emit('loginCompleted', username, color, base64Image);
			});
		});
	}

	setDefaultRandomColorPicked() {
		this.loginForm
			.find('.player-color')
			.attr('value', '#' + ((Math.random() * 0xffffff) << 0).toString(16));
	}

	loginAlreadyExistsError() {
		this.loginForm.find('.username').addClass('is-invalid');
	}

	show() {
		super.show();
	}

	hide() {
		super.hide();
	}
}

function arrayBufferToBase64(buffer) {
	var binary = '';
	var bytes = new Uint8Array(buffer);
	var len = bytes.byteLength;
	for (var i = 0; i < len; i++) {
		binary += String.fromCharCode(bytes[i]);
	}
	return window.btoa(binary);
}
