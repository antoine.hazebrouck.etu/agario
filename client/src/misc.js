import $ from 'jquery';
import Router from './Router';

export function midToMouseDistance(
	canvasMiddleX,
	canvasMiddleY,
	mouseX,
	mouseY
) {
	return Math.sqrt(
		(canvasMiddleX - mouseX) * (canvasMiddleX - mouseX) +
			(canvasMiddleY - mouseY) * (canvasMiddleY - mouseY)
	);
}

export function midToMouseAngle(canvasMiddleX, canvasMiddleY, mouseX, mouseY) {
	let dx = mouseX - canvasMiddleX;
	let dy = mouseY - canvasMiddleY;
	let angle = Math.atan2(dy, dx);
	angle *= 180 / Math.PI; // rads to degs
	if (angle < 0) angle = 360 + angle; // tourner l'anglage pour que 0deg soit a droite
	return angle;
}

export function removeEvents() {
	$(document).off('keydown');
	$(document).off('keyup');
}
