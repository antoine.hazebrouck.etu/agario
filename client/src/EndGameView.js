import View from './View.js';
import Router from './Router.js';

export default class EndGameView extends View {
	endGameBody;

	newPseudo;
	playAgain;

	currentPlayer;
	socket;

	constructor(element, endGameBody, newPseudo, playAgain, socket) {
		super(element);
		this.endGameBody = endGameBody;
		this.newPseudo = newPseudo;
		this.playAgain = playAgain;

		this.newPseudo.on('submit', event => {
			event.preventDefault();

			Router.navigate('/welcome', true);
		});

		this.playAgain.on('submit', event => {
			event.preventDefault();

			socket.emit(
				'loginCompleted',
				this.currentPlayer.name,
				this.currentPlayer.color,
				this.currentPlayer.image
			);
			Router.navigate('/', true);
		});
	}

	livedTime(player) {
		const milliseconds = Date.now() - player.creationDate;
		const seconds = Math.floor(milliseconds / 1000);
		return seconds;
	}

	scorePlayer(player) {
		const score = `<div class="infoPlayer"> <h4 class="score"> SCORE : ${player.score}</h4>`;
		const timeBetweenStartAndEaten = `
		<h5 class="time"> Vous avez survecu ${this.livedTime(player)} secondes</h5>`;

		const ediblesEaten = `
		<h5 class="eat"> Vous avez mangé ${player.peopleEat} nourritures </h5>`;
		this.endGameBody.html(
			'<div class="infoPlayer">' +
				score +
				timeBetweenStartAndEaten +
				ediblesEaten +
				'</div>'
		);
	}
}
