import $, { event } from 'jquery';
import io from 'socket.io-client';
import Router from './Router.js';
import ScoreboardView from './ScoreboardView.js';
import View from './View.js';
import WelcomeView from './WelcomeView.js';
import EndGameView from './EndGameView.js';
import {
	midToMouseAngle,
	midToMouseDistance,
	removeCreditsEvent,
	removeEvents,
	removeScoreboardEvent,
	setupCreditsEvent,
	setupScoreboardEvent,
} from './misc.js';
import render from './render.js';

const socket = io();
const canvas = document.querySelector('.gameCanvas');

// VIEWS
const welcomeView = new WelcomeView(
	$('.welcome'),
	$('.loginForm'),
	$('.image-preview'),
	$('.player-image'),
	socket
);
welcomeView.setDefaultRandomColorPicked();
const gameView = new View($('.game'));
const scoreboardView = new ScoreboardView(
	$('.scoreboard'),
	$('.scoreboard-body')
);
const creditView = new View($('.credits'));
const endGame = new EndGameView(
	$('.playAgain'),
	$('.infoPlayer'),
	$('.changePseudoForm'),
	$('.playAgainForm'),
	socket
);
// VIEWS

// ROUTER
const routes = [
	{ path: '/', view: gameView, title: 'Game' },
	{ path: '/welcome', view: welcomeView, title: 'Welcome' },
	{ path: '/scoreboard', view: scoreboardView, title: 'Scoreboard' },
	{ path: '/credits', view: creditView, title: 'Credits' },
	{ path: '/playAgain', view: endGame, title: 'PlayAgain' },
];
Router.routes = routes;
Router.navigate('/welcome', true);
// ROUTER

// LOGIN
socket.on('startLogin', () => {
	Router.navigate('/welcome', true);
});
socket.on('loginAlreadyExists', playerName => {
	welcomeView.loginAlreadyExistsError();
});
// LOGIN

// WHEN LOGGED IN
socket.on('loginAccepted', login => {
	Router.navigate('/');

	// CREDIT
	$(document).on('keydown', event => {
		if (event.key == 'c') {
			Router.navigate('/credits', true);
		}
	});
	$(document).on('keyup', event => {
		if (event.key == 'c') {
			Router.navigate('/', true);
		}
	});

	//SCOREBORD
	$(document).on('keydown', event => {
		if (event.key == 's') {
			Router.navigate('/scoreboard', true);
		}
	});
	$(document).on('keyup', event => {
		if (event.key == 's') {
			Router.navigate('/', true);
		}
	});

	// MOVEMENT
	const movementInfoToBeSent = { id: socket.id, angle: 0, speed: 0 };
	document.addEventListener('mousemove', event => {
		const distance = midToMouseDistance(
			canvas.width / 2,
			canvas.height / 2,
			event.clientX,
			event.clientY
		);
		const speed = (distance / 100) * 3;

		const angle = midToMouseAngle(
			canvas.width / 2,
			canvas.height / 2,
			event.clientX,
			event.clientY
		);

		movementInfoToBeSent.angle = angle;
		movementInfoToBeSent.speed = speed;
	});

	// // TOUT LES 20fps SEND LES INFOS MOVEMENT AU SERV
	setInterval(() => {
		socket.emit('moveXY', movementInfoToBeSent);
	}, 1000 / 20);
	// // TOUT LES 20fps SEND LES INFOS MOVEMENT AU SERV
	// MOVEMENT
});
// WHEN LOGGED IN

// // gestion des boutons précédent/suivant du navigateur (History API)
window.onpopstate = () => {
	Router.navigate(document.location.pathname, true);
};

socket.on('refresh', (players, edibles) => {
	scoreboardView.scoreboardUpdate(players);

	render(players, edibles, socket.id);
});

// END
socket.on('eaten', player => {
	// removeScoreboardEvent();
	removeEvents();
	// removeCreditsEvent();

	endGame.currentPlayer = player;
	endGame.scorePlayer(player);
	Router.navigate('/playAgain', true);
});
// END
