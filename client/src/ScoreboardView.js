import View from './View.js';
import $ from 'jquery';

export default class ScoreboardView extends View {
	scoreboardBody;

	constructor(element, scoreboardBody) {
		super(element);
		this.scoreboardBody = scoreboardBody;
	}
	scoreboardUpdate(players) {
		const sortedPlayers = players.sort((player1, player2) => {
			return player2.size - player1.size;
		});

		let boardString = '';
		let positionInBoard = 1;
		sortedPlayers.forEach(player => {
			boardString += `<tr><th scope="row">${positionInBoard}</th><td>${player.name}</td><td>${player.size}</td></tr>`;
			positionInBoard++;
		});
		this.scoreboardBody.html(boardString);
	}
}
