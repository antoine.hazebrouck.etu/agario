const canvas = document.querySelector('.gameCanvas');
const context = document.querySelector('.gameCanvas').getContext('2d');

const ZOOM_OUT_CYCLE = 100; // on zoom out tout les 100: size = 100 size = 200
const ZOOM_OUT_PROPORTION = 0.2; // on zoom out de 0.2 a chaque cycle

let scale = 1;
// let nextAmountOfCycles = 1;

const images = [];

function playerPossessesItsImageInImages(player, images) {
	const possibleCouple = images.find(couple => {
		return couple.socketId == player.socketId;
	});
	if (possibleCouple) return true;
	return false;
}

export default function render(players, edibles, socketId) {
	addImagesIfNotPresent(players, images);
	resizeCanvasToDisplaySize(canvas);
	clearCanvas(canvas, context);

	let offsetPlayerX = canvas.width / 2;
	let offsetPlayerY = canvas.height / 2;

	// LE JOUEUR ACTUEL
	const player = getPlayerBySocketId(players, socketId);

	// const newWidth = canvas.width * scale;
	// const newHeight = canvas.height * scale;

	// context.translate(
	// 	-((newWidth - canvas.width) / 2),
	// 	-((newHeight - canvas.height) / 2)
	// );
	// context.scale(scale, scale);

	// SI LE JOUEUR EST LOGGED IN
	if (player) {
		// ZOOM
		// const amountOfCyclesPassed = player.size / ZOOM_OUT_CYCLE;
		// if (sizePassesCycle(player.size, ZOOM_OUT_CYCLE, nextAmountOfCycles)) {
		// 	scale = zoomOut(ZOOM_OUT_PROPORTION, amountOfCyclesPassed);
		// 	nextAmountOfCycles++;
		// }
		// ZOOM
		offsetPlayerX = player.x;
		offsetPlayerY = player.y;
	}
	// LE JOUEUR ACTUEL

	// LES EDIBLES
	edibles.forEach(edible => {
		drawCirle(
			context,
			edible.x - offsetPlayerX + canvas.width / 2,
			edible.y - offsetPlayerY + canvas.height / 2,
			edible.size,
			edible.color
		);
	});
	// LES EDIBLES

	// LES AUTRES
	players.forEach(player => {
		const couple = images.find(couple => {
			return couple.socketId == player.socketId;
		});
		context.save();
		if (player.image) {
			context.beginPath();
			context.arc(
				player.x - offsetPlayerX + canvas.width / 2,
				player.y - offsetPlayerY + canvas.height / 2,
				player.size,
				0,
				2 * Math.PI,
				false
			);
			context.closePath();
			context.stroke();

			context.clip();
			context.drawImage(
				couple.image,
				player.x - offsetPlayerX + canvas.width / 2 - player.size,
				player.y - offsetPlayerY + canvas.height / 2 - player.size,
				player.size * 2,
				player.size * 2
			);

			context.font = `${player.size}px serif`;
			context.fillStyle = 'red';
			context.textAlign = 'center';
			context.fillText(
				player.name,
				player.x - offsetPlayerX + canvas.width / 2,
				player.y - offsetPlayerY + canvas.height / 2 + 3
			);
		} else {
			drawCirle(
				context,
				player.x - offsetPlayerX + canvas.width / 2,
				player.y - offsetPlayerY + canvas.height / 2,
				player.size,
				player.color,
				player.name
			);
		}
		context.restore();
	});
	// LES AUTRES

	// context.restore();
}

function addImagesIfNotPresent(players, images) {
	players.forEach(player => {
		if (!playerPossessesItsImageInImages(player, images)) {
			const playerImage = new Image();
			playerImage.src = 'data:image/png;base64,' + player.image;
			images.push({ socketId: player.socketId, image: playerImage });
		}
	});
}

function sizePassesCycle(size, baseCycleValue, nextAmountOfCycles) {
	// si l'amount de cycles calculé est > au last amount de cycles
	const amountOfCyclesPassed = size / baseCycleValue;
	return amountOfCyclesPassed > nextAmountOfCycles;
}

function zoomOut(zoomOutProportion, amountOfCyclesPassed) {
	// des qu'on prend 100size
	const scale =
		1 /
		(1.0 +
			zoomOutProportion *
				amountOfCyclesPassed); /* nb de fois qu'on a depassé 10 */ // dezoom de 20%
	return scale;
}

function clearCanvas(canvas, context) {
	context.clearRect(0, 0, canvas.width, canvas.height);
}

function drawCirle(context, x, y, size, color, name = '') {
	context.beginPath();
	context.arc(x, y, size, 0, 2 * Math.PI, false);
	context.fillStyle = color;
	context.closePath();
	context.stroke();
	context.fill();

	context.font = `${size}px serif`;
	context.fillStyle = 'red';
	context.textAlign = 'center';
	context.fillText(name, x, y + 3);
}

function drawImage(context, x, y, size, image, name = '') {
	context.beginPath();
	context.arc(x, y, size, 0, 2 * Math.PI, false);
	context.closePath();
	context.stroke();

	// context.clip();
	context.drawImage(image, x - size, y - size, size * 2, size * 2);

	context.font = `${size}px serif`;
	context.fillStyle = 'red';
	context.textAlign = 'center';
	context.fillText(name, x, y + 3);

	// context.restore();
}

function getPlayerBySocketId(players, socketId) {
	return players.find(player => player.socketId === socketId);
}

function resizeCanvasToDisplaySize(canvas) {
	// Lookup the size the browser is displaying the canvas in CSS pixels.
	const displayWidth = canvas.clientWidth;
	const displayHeight = canvas.clientHeight;

	// Check if the canvas is not the same size.
	const needResize =
		canvas.width !== displayWidth || canvas.height !== displayHeight;

	if (needResize) {
		// Make the canvas the same size
		canvas.width = displayWidth;
		canvas.height = displayHeight;
	}

	return needResize;
}
