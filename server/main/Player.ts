import Game from './Game';
import Piece from './Piece';

export type Color = string;
export type ID = string;
export type Base64 = string;

export default class Player extends Piece {
	static MAX_SIZE = 200;
	static MAX_SPEED = 10;
	static MIN_SPEED = 3;
	static PLAYER_DEBUT_SIZE = 15;

	socketId: ID;
	name: string;
	moveX: number;
	moveY: number;
	color: Color;
	image: Base64;
	score = 0;
	creationDate = Date.now();

	constructor(
		name: string,
		x: number,
		y: number,
		color: Color,
		image: Base64,
		socketId: ID
	) {
		super(x, y, Player.PLAYER_DEBUT_SIZE, 0);
		this.name = name;
		this.moveX = 0;
		this.moveY = 0;
		this.color = color;
		this.image = image;
		this.socketId = socketId;
	}

	movePlayer(borderX: number, borderY: number): void {
		// vertical
		this.y = this.y + this.moveY;
		if (this.y - this.size <= 0) {
			this.y = 0 + this.size;
		}
		if (this.y + this.size >= borderY) {
			this.y = borderY - this.size;
		}
		// horizontal
		this.x = this.x + this.moveX;
		if (this.x - this.size <= 0) {
			this.x = 0 + this.size;
		}
		if (this.x + this.size >= borderX) {
			this.x = borderX - this.size;
		}
	}

	/**
	 * fait grandir un player de la taille donné en parametre
	 * et verifie qu'il n'a pas atteint la taille maximal
	 */
	grow(taille: number): void {
		if (this.size < Player.MAX_SIZE) {
			this.size += taille;
		}
	}

	//Vitesse initiale * (Taille initiale / Taille actuelle)
	computeNextY(angle: number, speed: number, radius: number, size: number) {
		let nextY = radius * Math.sin((Math.PI * 2 * angle) / 360);
		return nextY * speed;
	}

	//Vitesse initiale * (Taille initiale / Taille actuelle)
	computeNextX(angle: number, speed: number, radius: number, size: number) {
		let nextX = radius * Math.cos((Math.PI * 2 * angle) / 360);
		return nextX * speed;
	}
	/**
	 * gere la collision entre 2 joueur en le faisant gradir et en augmentant son score
	 */
	collision(otherPlayerOrEdible: Piece): boolean {
		// la taille maximum
		if (this.canEat(otherPlayerOrEdible)) {
			this.grow(otherPlayerOrEdible.size * 2.25);
			this.score += otherPlayerOrEdible.getPoint();
			this.peopleEat += this.howMuchEat(otherPlayerOrEdible);
			return true;
		} else {
			return false;
		}
	}

	getPoint(): number {
		return (this.size * 5) / 15;
	}

	howMuchEat(otherPlayerOrEdible: Piece): number {
		return this.peopleEat + otherPlayerOrEdible.peopleEat;
	}

	nbTimesBiggerThanDebutSize() {
		return this.size / Game.PLAYER_DEBUT_SIZE;
	}
}
