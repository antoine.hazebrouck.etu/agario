export default abstract class Piece {
	x: number;
	y: number;
	size: number;
	color: string;
	peopleEat: number;

	constructor(x: number, y: number, size: number, peopleEat: number) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.color = '#' + ((Math.random() * 0xffffff) << 0).toString(16);
		this.peopleEat = peopleEat;
	}

	/**
	 * une Piece (nouriture ou player ) peut manger un autre s'il est minimum 25% plus grand
	 * s'il le touche et si l'autre joueur a deja mangé au moins une fois
	 */
	canEat(otherPiece: Piece): boolean {
		let dist = Math.sqrt(
			(otherPiece.x - this.x) ** 2 + (otherPiece.y - this.y) ** 2
		);
		if (
			this.size > otherPiece.size * 1.25 &&
			dist <= otherPiece.size + this.size &&
			otherPiece.peopleEat > 0
		) {
			return true;
		}
		return false;
	}

	/**
	 * retourne les points d'une Piece que l'adversaire gagnera
	 * pour une nouriture c'est 5 et pour un joueur c'est 5pt proportionelle à sa taille
	 */
	abstract getPoint(): number;

	/**
	 * retourne combien de player ou de nouriture une Piece à mangé
	 */
	abstract howMuchEat(otherPlayerOrEdible: Piece): number;
}
