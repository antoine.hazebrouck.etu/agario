import Edible from './Edible';
import Piece from './Piece';
import Player, { Base64, Color, ID } from './Player';

export default class Game {
	static MAX_EDIBLE = 55;
	static EDIBLE_SIZE = 5;
	static PLAYER_DEBUT_SIZE = 15;

	public players: Player[];
	edibles: Edible[];
	borderX: number;
	borderY: number;

	constructor(borderX: number, borderY: number) {
		this.players = [];
		this.edibles = [];
		this.borderX = borderX;
		this.borderY = borderY;
	}

	randomEdibleSpawn() {
		if (
			//this.edibles.length < this.borderX * this.borderY && pourquoi?
			this.edibles.length < Game.MAX_EDIBLE
		) {
			let randomX = Math.random() * this.borderX;
			let randomY = Math.random() * this.borderY;
			this.edibles.push(new Edible(randomX, randomY, Game.EDIBLE_SIZE));
		}
	}

	// newPlayer(playerName: string, color: Color, socketId: string): void {
	newPlayer(playerName: string, color: Color, image: Base64, socketId: ID): void {
		const newPlayer = new Player(
			playerName,
			Math.random() * this.borderX,
			Math.random() * this.borderY,
			color,
			image,
			socketId
		);
		this.players.push(newPlayer);
	}

	getPlayer(socketId: string): Player | undefined {
		return this.players.find(player => player.socketId === socketId);
	}

	getAllPieces(): Piece[] {
		return (this.players as Piece[]).concat(this.edibles as Piece[]);
	}
}
