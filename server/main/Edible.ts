import Piece from './Piece';

export default class Edible extends Piece {
	constructor(x: number, y: number, size: number) {
		super(x, y, size, 1);
	}

	getPoint(): number {
		return 5;
	}

	howMuchEat(otherPlayerOrEdible: Piece): number {
		return 1;
	}
}
