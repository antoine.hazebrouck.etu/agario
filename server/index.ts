import express from 'express';
import http from 'http';
import { Server as IOServer } from 'socket.io';
import Edible from './main/Edible';
import Game from './main/Game';
import Player from './main/Player';
import addWebpackMiddleware from './main/addWebpackMiddleware.js';

const NEXT_POS_RADIUS = 1;
const FLATTEN_SLOW_CURVE_BY = 275;

// game
const game = new Game(1000, 1000);
const app = express();
const httpServer = http.createServer(app);
addWebpackMiddleware(app);

const io = new IOServer(httpServer);
io.on('connection', socket => {
	console.log(`Nouvelle connexion du client ${socket.id}`);
	socket.emit('startLogin');
	socket.on('loginCompleted', (playerName, color, image) => {
		// WHEN LOGGED IN
		if (!nameAlreadyUsed(playerName, game.players)) {
			socket.emit('loginAccepted', playerName);
			game.newPlayer(playerName, color, image, socket.id);
			const player = game.getPlayer(socket.id);
			socket.on('moveXY', infoMovement => {
				// on verifie que le joueur qui veut bouger est bien vivant
				if (player) {
					const nbTimesBiggerThanDebutSize =
						player.nbTimesBiggerThanDebutSize();

					let slowedBy =
						nbTimesBiggerThanDebutSize ** 2 / FLATTEN_SLOW_CURVE_BY;
					slowedBy = floorNumberDownTo(slowedBy, 0.5);

					let speed = floorNumberDownTo(infoMovement.speed, Player.MAX_SPEED);
					speed = speed * (1 - slowedBy);

					player.moveX = player.computeNextX(
						infoMovement.angle,
						speed,
						NEXT_POS_RADIUS,
						player.size
					);
					player.moveY = player.computeNextY(
						infoMovement.angle,
						speed,
						NEXT_POS_RADIUS,
						player.size
					);
				}
				// LE JOUEUR QUI BOUGE
			});
		}

		// WHEN LOGGED IN
		else {
			socket.emit('loginAlreadyExists');
		}
	});

	socket.on('disconnect', () => {
		// on gere le fait qu'un joueur mort raffraichisse la page
		// et le fait qu'un joueur vivant soit supprimer de la liste des joueurs
		const player = game.getPlayer(socket.id);
		if (player) {
			for (let i = 0; i < game.players.length; i++) {
				if (game.players[i].socketId === player.socketId) {
					game.players.splice(i, 1);
				}
			}
		}
		console.log(`Déconnexion du client ${socket.id}`);
	});

	//reuni les edible et les players dans une meme liste
	// on suprime du tableau des edible les edible mangé
	//on suprime du tableau des player les player mangé

	//refactor splice abstract
	setInterval(() => {
		const player = game.getPlayer(socket.id);
		game.getAllPieces().forEach(piece => {
			if (player) {
				// si l'autre mange le joueur
				if (piece instanceof Player && piece.canEat(player)) {
					const otherPlayer: Player = piece;
					otherPlayer.grow(otherPlayer.size * 2.25); // TODO
					otherPlayer.score += otherPlayer.getPoint();
					otherPlayer.peopleEat += otherPlayer.howMuchEat(otherPlayer);
					socket.emit('eaten', player);
					game.players.splice(game.players.indexOf(player), 1);
				}

				// si le joueur mange l'edible
				if (piece instanceof Edible && player.canEat(piece)) {
					player.grow(piece.size * 2.25);
					player.score += piece.getPoint();
					player.peopleEat += player.howMuchEat(piece);
					game.edibles.splice(game.edibles.indexOf(piece), 1);
				}
			}
		});
	}, 1000 / 20);
});

// A FAIRE afficher à l'utilisateur que la taille du pseudo est trop grande
// nameAlreadyUsed marche pas (on rentre dans le true mais return quand meme false)
function nameAlreadyUsed(playerName: string, players: Player[]) {
	if (playerName === '') {
		return true;
	}
	return players.find(player => player.name === playerName);
}

function floorNumberDownTo(number: number, to: number): number {
	if (number > to) return to;
	return number;
}

// TOUTES LES 20fps BROADCAST LE JEU
setInterval(() => {
	io.emit('refresh', game.players, game.edibles);
	game.players.forEach(player => {
		player.movePlayer(game.borderX, game.borderY);
	});
}, 1000 / 20);
// TOUTES LES 20fps BROADCAST LE JEU

// TOUTES LES 0.5s NEW EDIBLE
setInterval(() => {
	game.randomEdibleSpawn();
}, 1000 / 0.5);
// TOUTES LES 0.5s NEW EDIBLE

app.use(express.static('client/public'));

const port = '8000';
httpServer.listen(port, () => {
	console.log(`Server running at http://localhost:${port}/`);
});
